Thông tin chi tiết:

Tham khảo: https://hiyams.com/may-xay/may-xay-sinh-to/may-xay-sinh-to-tefal/may-xay-sinh-to-tefal-bl47yb66/

Máy xay sinh tố Tefal BL47YB66 được thiết kế nhỏ gọn đa năng. Không những mang đến những ly sinh tố thơm ngon và tiện lợi trong việc xay nhuyễn thực phẩm. mà còn giúp tô điểm thêm cho quầy bếp thêm sang trọng với màu trắng xám đẹp mắt.
Máy xay sinh tố Tefal BL47YB66 với 3 cối đi kèm tiện lợi hỗ trợ xay nhiều loại thực phẩm khác nhau.
Máy xay sinh tố Tefal BL47YB66 động cơ máy xay sinh tố Tefal mạnh mẽ kết hợp cùng lưỡi dao 6 cánh ứng dụng công nghệ Powelix. Dễ dàng xay mịn thực phẩm và tiết kiệm thời gian.

Thông số kỹ thuật Máy xay sinh tố Tefal BL47YB66:
Công suất: 800W
3 Cối Xay: Cối nhựa lớn + Cối nhỏ xay tiêu + Cối lớn XL xay thịt
Động cơ xay: Powelix
Số lưỡi dao: 6 cánh
Chức năng: Xay sinh tố, rau củ quả, xay thịt, xay tiêu
Tốc độ xay: 5 tốc độ+ 1 nhồi
Dung tích tổng: 2 L
Dung tích sử dụng: 1.25 L
Xuất xứ: Trung Quốc
Kích thước: 51.1 x 21.1 x 30.1 cm (DxRxC)
Khối lượng: 2.774 kg
Bảo hành: 2 năm